<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>inscription</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>
<body>
	<style>
		.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar {
		    display: inline-block;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    width: 213px;
		}
		.kv-reqd {
		    color: red;
		    font-family: monospace;
		    font-weight: normal;
		}
	</style>
	<div class="container" style="background-color: #87CEEB">
		<div class="col-md-12" style="margin-top: 50px">
			<h1 style="text-align: center;">Inscrivez vous rapidement</h1>
		</div>
		<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
		<form class="form form-vertical" action="formtraitement.php" method="post" enctype="multipart/form-data" style="margin-top: 200px;">
		    <div class="row">
		    	<div class="col-sm-4 text-center">
		    		<div class="row">		
				        <div class="">
				            <div class="kv-avatar">
				                <div class="file-loading">
				                	<label for="avatar-2">Photo de profil:</label>
				                    <input type="file" name="avatar-2" id="avatar-2" class="form-control" accept="image/*" onchange="loadFile(event)" required="">
				                </div>
				            </div>
				            <div class="kv-avatar-hint">
				                <small>Select file < 1500 KB</small>
				            </div>
				        </div>
				        <div class="col-md-offset-4 col-md-4" style="margin-top: 10px; margin-bottom: 10px; height: 120px; width: 120px; border-radius: 50%;">
                             <img id="pp" style="height: 120px; width: 120px; border-radius: 30%;" />
                      </div>
		    		</div> 
		    	</div>
		        <div class="col-sm-8">
		            <div class="row">
		            	<div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="fname">First Name<span class="kv-reqd">*</span></label>
		                        <input type="text" class="form-control" name="fname" required>
		                    </div>
		                </div>
		            	 <div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="lname">Last Name<span class="kv-reqd">*</span></label>
		                        <input type="text" class="form-control" name="lname" required>
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="email">Email Address<span class="kv-reqd">*</span></label>
		                        <input type="email" class="form-control" name="email" required>
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="pwd">Password<span class="kv-reqd">*</span></label>
		                        <input type="password" class="form-control" name="pwd" required>
		                    </div>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-sm-6">
		                	<div class="form-group">
		                		<label for="tel">Tel<span class="kv-reqd">*</span></label>
								<input class="form-control" type="text" name="tel">
		                	</div>
		                </div>
		                <div class="col-sm-6">
		                	<div class="form-group">
		                		<label>Date de Naissance<span class="kv-reqd">*</span></label>
				                <input class="form-control" type="Date" name="dat">
		                	</div>
		                </div>
		               
		                <div class="col-sm-6">
		                	<div class="form-group">
		               			<label  for="pays">Pays<span class="kv-reqd">*</span></label>
								<input type="text" class="form-control" name="pays">
		                	</div>
		                </div>
		                <div class="col-sm-6">
		                	<div class="form-group">
		                		<label>Sexe<span class="kv-reqd">*</span></label>
								<select class="form-control" name="sex">
									<option selected="selected" disabled="">choix du sexe</option>
									<option name="Masculin">Masculin</option>
									<option name="Feminin">Feminin</option>
								</select>
		                	</div>
		                </div>
		            </div>
		            <div class="form-group">
		                <hr>
		                <div class="col-md-12 text-right">
		                    <div style="width: 100% !important"><button type="submit" class="btn btn-primary" style="width: 100% !important;">S'inscrire</button></div>
		                    <div><center><p><a href="connexion.php" style="color: black; text-decoration: none">j'ai deja un compte</a></p></center> </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</form>
	</div>

    <script>
		var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' + 
		    'onclick="alert(\'Call your custom code here.\')">' +
		    '<i class="glyphicon glyphicon-tag"></i>' +
		    '</button>'; 
		$("#avatar-2").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 1500,
		    showClose: false,
		    showCaption: false,
		    showBrowse: false,
		    browseOnZoneClick: true,
		    removeLabel: '',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-2',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="/samples/default-avatar-male.png" alt="Your Avatar"><h6 class="text-muted">Click to select</h6>',
		    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
	</script>
	<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
 	</script>

<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>