<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>connexion</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>
<body>
	<style>
		.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
		    margin: 0;
		    padding: 0;
		    border: none;
		    box-shadow: none;
		    text-align: center;
		}
		.kv-avatar {
		    display: inline-block;
		}
		.kv-avatar .file-input {
		    display: table-cell;
		    width: 213px;
		}
		.kv-reqd {
		    color: red;
		    font-family: monospace;
		    font-weight: normal;
		}
	</style>
	<div class="container" style="background-color: #87CEEB">
		<div class="col-md-12" style="margin-top: 50px">
			<h1 style="text-align: center;">Connectez vous rapidement</h1>
		</div>
		<form class="form form-vertical" action="/site/avatar-upload/2" method="post" enctype="multipart/form-data" style="margin-top: 150px;">
		    <div class="row">
		    	<div class="col-sm-2"></div>
		        <div class="col-sm-8">
		            <div class="row">
		                <div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="email">Email Address<span class="kv-reqd">*</span></label>
		                        <input type="email" class="form-control" name="email" required>
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                    <div class="form-group">
		                        <label for="pwd">Password<span class="kv-reqd">*</span></label>
		                        <input type="password" class="form-control" name="pwd" required>
		                    </div>
		                </div>
		            </div>
		            <div class="form-group">
		                <hr>
		                <div class="col-md-12 text-right">
		                    <div style="width: 100% !important"><button type="submit" class="btn btn-primary" style="width: 100% !important;">Connexion</button></div>
		                    <div class="form-group">
							    <div class="form-check">
							      <input class="form-check-input" type="checkbox" id="gridCheck">
							      <label class="form-check-label" for="gridCheck">
							        se souvenir de moi
							      </label>
							    </div>
							</div>
		                    <div><center><p><a href="form.php" style="color: black; text-decoration: none">je n'ai pas encore un compte</a></p></center> </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</form>
	</div>

</body>
</html>